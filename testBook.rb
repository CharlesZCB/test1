require 'time'
class Book
  MY_BOOK=Array[{"title"=>"A1","describe"=>"B1","book_type"=>1,"price"=>50,"published_at"=>"2014-01-01 01:01:01"},
                {"title"=>"A2","describe"=>"B2","book_type"=>2,"price"=>75,"published_at"=>"2015-01-02 01:01:01"},
                {"title"=>"A3","describe"=>"B3","book_type"=>3,"price"=>100,"published_at"=>"2016-01-01 01:01:01"}]
  attr_accessor :title
  attr_accessor :describe
  attr_accessor :book_type
  attr_accessor :price
  attr_accessor :published_at
#test alert  shgsdufsjfsdkg
  def initialize(hash)   #定义初始化方法
    self.title = hash['title']
    self.describe=hash['describe']
    self.book_type=hash['book_type']
    self.price=hash['price']
    self.published_at=hash['published_at']

  end

  def create_some_books
    @b1 = Book.new(MY_BOOK.at(0)["title"],MY_BOOK.at(0)["describe"],MY_BOOK.at(0)["book_type"],MY_BOOK.at(0)["price"],MY_BOOK.at(0)["published_at"])
    @b2 = Book.new(MY_BOOK.at(1)["title"],MY_BOOK.at(1)["describe"],MY_BOOK.at(1)["book_type"],MY_BOOK.at(1)["price"],MY_BOOK.at(1)["published_at"])
    @b3 = Book.new(MY_BOOK.at(2)["title"],MY_BOOK.at(2)["describe"],MY_BOOK.at(2)["book_type"],MY_BOOK.at(2)["price"],MY_BOOK.at(2)["published_at"])
  end

  def price_level  #返回价位
    if self .price>=0 && self.price<60
      return "低价位"
    elsif self.price<90
      return "中价位"
    else
      return "高价位"
    end
  end

  def type_name
    case self.book_type
    when 1
      return "旅游类"
    when 2
      return "技术类"
    else
      return "军事类"
    end
  end

  def self.high_price_book_price
    total_price=0
    MY_BOOK.each do |object|
      if object["price"]>=60
        total_price+=object["price"]
      end
    end
    return total_price
  end

  def month_end  #返回该年月末值
    #方法1
    time = Time.local(self .published_at)
    return (time - 1).day

    #方法2  取年月  进行判断
    # if year%400==0||(year%4==0&&year%100!=0)  #润年
    #   if month==1||month==3||month==5||month==7||month==8||month==10||month==12
    #     return 31
    #   elsif month==4||month==6||month==9||month==11
    #     return 30
    #   else
    #     return 29
    #   end
    # elsif year%4!=0 && (year%400!=0&&year%100==0)
    #   if month==1||month==3||month==5||month==7||month==8||month==10||month==12
    #     return 31
    #   elsif month==4||month==6||month==9||month==11
    #     return 30
    #   else
    #     return 28
    #   end
    # end
  end


  def published_at_cn   #返回制定格式的日期
    #puts self .published_at
    times=Time.parse(self.published_at).strftime("%Y-%m-%d")
    return  times
  end

  def self.avg_price    #返回MY_BOOK中的书的价格平均数
    total=0
    number=0
    MY_BOOK.each do |object|
      if object["price"]>=60
        total+=object["price"]
        number+=1
      end
    end
    return total/number
  end



end
Book::MY_BOOK.each do |book|
  book=Book.new(book)
  puts book.published_at_cn
  puts book.month_end
  puts book.price_level
  puts book.type_name
end

puts "The average price about higher 60 is:#{Book.avg_price}"
puts "The total price about higher 60 is:#{Book.high_price_book_price}"


# puts book.published_at_cn(Time.new,"%Y  %m  %d  ")
# puts book.month_end(2016,2)
# puts book.price_level(99)
# puts book.type_name(3)
# book=Book.new()
#
# puts book.avg_price

